FROM guillaumebriday/ruby-node:2.6.3

WORKDIR /app

RUN gem install bundler -v 2.4.22

COPY Gemfile .
COPY Gemfile.lock .

RUN bundle install

COPY . .

CMD bundle exec middleman server
