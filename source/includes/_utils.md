# Hestia Utils

The utils library contains useful functions to work with Hestia data.

## Pivoting Headers by Terms

After downloading data as CSV from the Hestia platform, the format will look like this:

| site.@id | site.measurements.0.term.@id | site.measurements.0.value | site.measurements.1.term.@id | site.measurements.1.value | site.measurements.2.term.@id | site.measurements.2.value | site.dataPrivate |
|-----|-----|-----|-----|-----|-----|-----| -----|
| xvflr | sandContent | 90 | siltContent | 6 | clayContent | 4 | false |
| gght | sandContent | 90 | siltContent | 6 | clayContent | 4 | false |

It is possible to pivot some data based on the `term.@id` and move them as columns, such as:

| site.@id | site.measurements.sandContent.value | site.measurements.siltContent.value | site.measurements.clayContent.value | site.dataPrivate |
| ----- |-----|-----|------| -----|
| xvflr | 90 | 6 | 4 | false |
| gght | 90 | 6 | 4 | false |

### Usage

1. Install `Python` version `3` minimum
2. Install the data validation library: `pip install hestia_earth.utils`
3. Run:

<div class="full-column"></div>

```
hestia-pivot-csv source.csv dest.csv
```

The `dest.csv` file will be pivoted.
