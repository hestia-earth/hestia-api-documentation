# Hestia Calculation Models

The Calculation Models are a set of modules for creating structured data models from LCA observations and evaluating biogeochemical aspects of specific farming cycles.

## Usage

1. You will need to use python `3` (we recommend using python `3.6` minimum).
2. Install the library:

<div class="full-column"></div>

```bash
pip install hestia_earth.models
```

3. Set the following environment variables:

<div class="full-column"></div>

```bash
API_URL="<APIUrl>"
WEB_URL="<WEBUrl>"
```

Now you can scroll to the model you are interested in and follow the instructions to run them.

<aside class="notice">
  Every model has a set of "Requirements" to be able to run. Please look at the related schema to format your data accordingly or the model will not run.
</aside>

### Logging

The models library is shipped with it's own logging which will be displayed in the console by default.
If you want to save the logs into a file, please set the `LOG_FILENAME` environment variable to the path of the file when running the models.

Example with a `my_file.py` file like:

<div class="full-column"></div>

```python
from hestia_earth.models.pooreNemecek2018 import run

run('no3ToGroundwaterSoilFlux', cycle_data)
```

You can save the output in the `models.log` file by running `LOG_FILENAME=models.log python my_file.py`.

### Orchestrator

Hestia has developed a library to run the models in a specific sequence defined in a configuration file called the [Hestia Engine Orchestrator](https://gitlab.com/hestia-earth/hestia-engine-orchestrator).

Whereas when running a single model you would do:

<div class="full-column"></div>

```python
from hestia_earth.models.pooreNemecek2018 import run

run('no3ToGroundwaterInorganicFertiliser', cycle_data)
```

You can run a sequence of models by doing instead:

<div class="full-column"></div>

```python
from hestia_earth.orchestrator import run

config = {
  "models": [
    {
      "key": "emissions",
      "model": "pooreNemecek2018",
      "value": "no3ToGroundwaterInorganicFertiliser",
      "runStrategy": "add_blank_node_if_missing"
    },
    {
      "key": "emissions",
      "model": "pooreNemecek2018",
      "value": "no3ToGroundwaterOrganicFertiliser",
      "runStrategy": "add_blank_node_if_missing"
    }
  ]
}

run(cycle_data, config)
```

More information and examples are available in the [Hestia Engine Orchestrator repository](https://gitlab.com/hestia-earth/hestia-engine-orchestrator).
