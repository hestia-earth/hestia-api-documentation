;(function () {
  'use strict';

  function waitForNavbar(callback) {
    if (document.getElementById('navbar-dd')) {
      callback();
    }
    else {
      setTimeout(function() {
        waitForNavbar(callback);
      }, 100);
    }
  }

  function setup() {
    var activeClass = 'is-active';
    var navbarButtons = document.querySelectorAll('[data-target]');
    var navbarActive = [];
    navbarButtons.forEach(function () {
      navbarActive.push(false);
    });
    var navbarMenus = document.querySelectorAll('.navbar-menu');

    function handleMenuToggle(button, index) {
      var target = button.dataset.target;
      var navbarMenu = document.getElementById(target);
      button.addEventListener('click', function (ev) {
        // make sure all menus are hidden first
        navbarButtons.forEach(function (el, bIndex) {
          navbarActive[bIndex] = false;
          el.classList.remove(activeClass);
        });
        navbarMenus.forEach(function (el) {
          el.classList.remove(activeClass);
        });

        setTimeout(function () {
          navbarActive[index] = !navbarActive[index];
          if (navbarActive[index]) {
            button.classList.add(activeClass);
            navbarMenu.classList.add(activeClass);
          }
          else {
            button.classList.remove(activeClass);
            navbarMenu.classList.remove(activeClass);
          }
        });
      });
    }

    // open/close menu on mobile
    navbarButtons.forEach(handleMenuToggle);

    // user handling
    var hiddenClass = 'is-hidden';
    var storageKey = 'he_ws-he_';

    window.signOut = function() {
      window.localStorage.clear(storageKey + 'p');
      window.localStorage.clear(storageKey + 't');
      window.location.reload();
    };

    var user = window.localStorage.getItem(storageKey + 'p');
    var unlogged = document.querySelectorAll('.user-unlogged');
    var logged = document.querySelectorAll('.user-logged');
    var usernames = document.querySelectorAll('.user-name');
    if (user && user.length) {
      user = JSON.parse(user);
      var initials = user.firstName[0] + (user.lastName ? user.lastName[0] : '.');
      usernames.forEach(function (e) {
        e.textContent = initials;
      });

      unlogged.forEach(function (e) {
        e.classList.add(hiddenClass);
      });
      logged.forEach(function (e) {
        e.classList.remove(hiddenClass);
      });
    }
    else {
      unlogged.forEach(function (e) {
        e.classList.remove(hiddenClass);
      });
      logged.forEach(function (e) {
        e.classList.add(hiddenClass);
      });
    }
  }

  waitForNavbar(setup);
})();
