window.getUser = function getUser() {
  var user = window.localStorage.getItem('he_ws-he_p');
  return user && user.length ? user : null;
};

window.getApiKey = function getApiKey() {
  var key = window.localStorage.getItem('he_ws-he_t');
  return key ? JSON.parse(key) : null;
};
